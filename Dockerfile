FROM nginx:latest

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y certbot cron incron openssl

COPY conf/incron.allow /etc/incron.allow
COPY conf/nginx.incrontemplate /root/nginx.incrontemplate

COPY conf/certbot.crontab /etc/cron.d/certbot.cron
RUN chmod 0644 /etc/cron.d/certbot.cron

COPY conf/nginx.template /root/nginx.template

COPY conf/bootstrap.sh /root/bootstrap.sh
RUN chmod +x /root/bootstrap.sh

ENTRYPOINT ["/bin/bash", "-c", "/root/bootstrap.sh"]