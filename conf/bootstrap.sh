#!/bin/bash

if [ ! -f /etc/letsencrypt/certs/${CERTBOT_DOMAIN}.key ]; then
    mkdir -p /etc/letsencrypt/certs

    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=Temp/L=Temp/O=Temp/CN=${CERTBOT_DOMAIN}" -keyout /etc/letsencrypt/certs/${CERTBOT_DOMAIN}.key  -out /etc/letsencrypt/certs/${CERTBOT_DOMAIN}.cert
    wait
fi

service cron start

envsubst \${CERTBOT_DOMAIN} < /root/nginx.template > /etc/nginx/nginx.conf
wait

envsubst "$${CERTBOT_DOMAIN}" < /root/nginx.incrontemplate > /etc/incron.d/nginx.incrontab
wait

service incron start

echo "Starting nginx"

nginx -g 'daemon off;'
