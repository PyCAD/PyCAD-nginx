user  nginx;
worker_processes  5;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    gzip on;

    server {
        listen       8080;
        server_name  ${CERTBOT_DOMAIN};
        access_log   off;

        location / {
            return 301 https://$server_name$request_uri;
        }

        location /.well-known {
            root    /var/www;
        }
    }

    server {
        listen 8443 ssl http2;
        server_name  ${CERTBOT_DOMAIN};

        ssl_certificate /etc/letsencrypt/certs/${CERTBOT_DOMAIN}.cert;
        ssl_certificate_key /etc/letsencrypt/certs/${CERTBOT_DOMAIN}.key;

        location / {
            root    /var/www;
        }

        location /api {
            proxy_pass http://api:8000;
        }
    }
}
